# Hello Neon Layout

## General info
This project contains simple layout for practice HTML and CSS.

## Technologies
* HTML
* CSS

## Status
Project is finished.
If You have any advice and ideas don't hesitate to contact me.

## Credits
Project inspired by theawwwesomes.org

## Where You can find me
[My blog about dietetics](https://healthyfitplace.blogspot.com)

[My blog on Instagram](https://www.instagram.com/healthyfitplace/)

[My blog on Facebook](https://www.facebook.com/HealthyFitPlaceblog/)

[My LinkedIn profile](linkedin.com/in/patrycja-surmela/)

Email: _patrycjasurmela@wp.pl_

